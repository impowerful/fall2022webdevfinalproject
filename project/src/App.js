
import Header from "./AppComponents/Header";
import Body from "./AppComponents/Body";
import Footer from "./AppComponents/Footer";
import { useState } from "react";
import './App.css';

function App() {
  const [filter, setFilter] = useState("");

  return (
    <div id="main">
      <Header onSearch={setFilter} />
      <Body searchFilter={filter} />
      <Footer />
    </div>
  );
}

export default App;
