/**
 * @author Yu Hua Yang 2133677
 */
import{useState, useEffect} from "react"
import ListPosts from "../PostComponents/ListPosts";
import DropDownCategories from "../DropDownComponents/DropDownCategories";
import DropDownTopics from "../DropDownComponents/DropDownTopics";

const ColumnMain = function({ searchFilter, object, topicList, listPosts }) {

//variable initialization

const [currentTopicList, setTopicList] = useState([]);
const [currentListPosts, setListPosts] = useState([]);
const [selectedCategory, setSelectedCategory] = useState();
const [filter, setFilter] = useState('')

//initialized hardcoded categories 

const [categories, setCategories] = useState([
  "Category 1 - Coding - Computer Science",
  "Category 2 - Project Management",
  "Category 3 - Communication",
]);

//sets the initial values shown on the main table

  useEffect(() => {
    setSelectedCategory(categories[0]);
    const topics = object.filter((o) => {
      return o.name == categories[0];
    })[0]["topicList"];
    setTopicList(topics);

    const posts = topics.filter((topic) => {
      return topic == topics[0];
    })[0]["listPosts"];
    setListPosts(posts);
  }, []);

  //whenever the topic is changed this function is called

  useEffect(() => {
    topicList(currentTopicList)
  }, [currentTopicList])

    //whenever the listPosts is changed this function is called
  useEffect(() => {
    listPosts(currentListPosts)
  }, [currentListPosts])

  //handles everytime a category is changed it changes the default posts shown
  const onCategoryChanged = (e) => {
    console.log(e.target.value);
    const category = e.target.value;
    setSelectedCategory(category);
    const topics = object.filter((o) => {
      return o.name == category;
    })[0]["topicList"];
    console.log(topics);
    setTopicList(topics);

    const posts = topics.filter((topic) => {
      return topic == topics[0];
    })[0]["listPosts"];
    setListPosts(posts);
  };

   //handles everytime a topic is changed it changes the default posts shown
  const onTopicChanged = (e) => {
    const topic = e.target.value;
    console.log(topic);
    const posts = currentTopicList.filter((t) => {
      return t.topic_title == topic;
    })[0]["listPosts"];
    setListPosts(posts);
  };

  //filters through the lists of posts depending on what was inputed in the search bar
  const filteredListPost = currentListPosts.filter(listpost => {
    return (
      listpost.author.indexOf(searchFilter) != -1 ||
      listpost.text.indexOf(searchFilter) != -1
    )
  })

//inputing all the information across to the other sections
return (
  <fieldset id="mainColumn"> 
      <div>
        <div id="selectors">
          <DropDownCategories
            options={categories}
            onSelectionChange={onCategoryChanged}
          />
          <DropDownTopics
            options={currentTopicList.map((t) => t.topic_title)}
            onSelectionChange={onTopicChanged}
          />
        </div>
      <div id="allPosts">
        {
        <ListPosts posts={filteredListPost}/>
        }
      </div>
    </div>
  </fieldset>
)
}

export default ColumnMain;

