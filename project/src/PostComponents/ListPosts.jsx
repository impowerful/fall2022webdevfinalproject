/**
 * @author Amatta Siripraphanh 2039170
 */
import React, { useState } from 'react';
    
const FillPost = ({ post }) => {

    //setups the likes and dislikes through images on the post

    let [likeValue,setLike] = useState(post.like);
    let [show,setShow] = useState(true);
    let [deleted,setDelete] = useState(true);
                return(
                    <>{deleted ?
                        <div className="ColumnMain">
                        <>
                            {show ?
                            <>
                                <div id="firstLine">
                                    <p id="firstParagraph">{post.text}</p>
                                        <div id="likes">
                                            <p id="likeCounter">Likes: {post.like}</p>
                                            <button id="likeBtn" onClick={handleLike}></button>
                                            <button id="dislikeBtn" onClick={handleDisLike}></button>
                                        </div>
                                    </div>
                                <div id="secondLine">
                                    <p id="secondParagraph">{post.author}</p>
                                    <p>{post.date}</p>
                                    <p>Replies: {post.replies}</p>
                                    <button id="deleteBtn" onClick={handleDelete}></button>
                                </div>
                                </>
                                :
                                <div >
                                    <h1 className="deletePrompt">POST WILL BE DELETED</h1>
                                    <div className="buttonPrompt">
                                        <button id="showBtn" onClick = {handleDelete}>Undo</button>
                                        <button id="clearBtn" onClick = {clearPost}>Delete</button>
                                    </div>
                                </div>
                            }
                            </>
                        </div> 
                        : 
                        <></>}
                        </>  
                )
        
    function handleDisLike() {
        setLike(likeValue -1)
        post.like = likeValue
    } 
    
    function handleLike() {
        setLike(likeValue+1)
        post.like = likeValue
    }
    
    function handleDelete() {
        setShow(!show)
    }
    
    function clearPost() {
        setDelete(!deleted)
    }
}

//runs the list posts and maps through every post to show it on main
function ListPosts(props) {
    return (
        <>
          {props.posts.map((post) => {
            return (
              <FillPost post={ post }  />
            );
          })}
        </>
      );
}

export default ListPosts;