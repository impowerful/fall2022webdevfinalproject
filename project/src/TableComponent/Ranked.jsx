/**
 * @author Amatta Siripraphanh 2039170
 */
//takes all the topics and maps through all the the information
function rankedTopics(topicList) {
    let rankedTopic = [];

    topicList.map( listing => {
        listing.topicList.forEach(topic => {
            rankedTopic.push(topic);
        })
    })
    rankedTopic.sort((a,b) => a.nberPost < b.nberPost)
return(
    <tbody>
    {
    rankedTopic.map(elem => {
        return (
            <tr>
                <td>{elem.topic_title}</td>
                <td>{elem.nberPost}</td>
                <td>{elem.status}</td>
            </tr>
        )
  
    })}
    </tbody>
    )
}

//creates the table for the tableClass
function Ranked(props) {
return (
    <>
    <table className="tableClass">
        <thead>
            <tr>
                <th>Topic Title</th>
                <th># of Post</th>
                <th>Status</th>
            </tr>
        </thead>
        {rankedTopics(props.topicList)}
    </table></>
);
}

export default Ranked;