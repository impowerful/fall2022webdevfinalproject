/**
 * @author Amatta Siripraphanh 2039170
 */
//add a drop down topics where you can filter through the topics
function DropDown({ options, onSelectionChange }) {
    return (
    <div className="selDiv">
        <label for="topic">Choose Topic:</label>
            <select name="topic" id="topics" onChange={onSelectionChange}>
                    {
                        options.map(option => {
                            return (
                                <option>
                                    {option}
                                </option>
                            )
                        })
                    }
            </select> 
    </div>
    )
  }

  export default DropDown;