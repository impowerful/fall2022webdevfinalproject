/**
 * @author Yu Hua Yang 2133677
 */
//add a drop down categories where you can filter through the catergories
 function DropDown({ options, onSelectionChange }) {
  return (
    <div className="selDiv">
    <label for="category">Choose Category:</label>
      <select name="category" id="categories" onChange={onSelectionChange}>
            {
                options.map(option => {
                    return (
                        <option>
                            {option}
                        </option>
                    )
                })
            }
      </select> 
    </div>
  )
}

export default DropDown;